import { Component, OnInit } from '@angular/core';
import { MENU, PATH } from '../../types/routing.type';
import { MenuService } from '../../services/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  activeRoute: MENU = MENU.HOME;

  protected readonly PATH = PATH;
  protected readonly MENU = MENU;

  constructor(private menuService: MenuService, private router: Router) {
  }

  ngOnInit(): void {
    this.menuService.getActiveRoute().subscribe((route) => this.activeRoute = route);
  }

  navigate(path: string): void {
    this.router.navigate([`${ path }`]);
  }
}
