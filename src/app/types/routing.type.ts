export enum MENU {
  HOME = 'HOME', PLAN = 'PLAN', MANAGE = 'MANAGE'
}

export enum PATH {
  HOME = 'home',
  PLANNED_LIST = 'planned',
  NEW_PLAN = 'create-plan',
  MANAGE = 'management'
}
