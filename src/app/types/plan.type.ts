import { IngredientSimple } from './ingredients.type';
import { DishSimple } from './dish.type';

export enum WeekDay {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
  OTHER = 'OTHER'
}

export type PlansCompressed = {
  id: string;
  plans: PlannedDishesSimple[];
}

export type PlannedDishesSimple = {
  from: string;
  to: string;
  dishes: PlannedDish[];
  otherIngredients?: string[];
}

export type PlannedDish = {
  day: WeekDay;
  dish: DishSimple //todo change structure
}

export interface NewPlanDialogData {
  isEdit: boolean;
  currentPlan?: PlannedDishesSimple;
  dishes: DishSimple[];
}
