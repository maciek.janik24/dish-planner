export type MessagesType = {
  key: string;
  message: string;
}


export const messageStore: MessagesType[] =
  [
    {
      key: 'required',
      message: 'This field is required'
    },
    {
      key: 'min',
      message: 'Field value is too small'
    },
    {
      key: 'max',
      message: 'Field value is too big'
    },
    {
      key: 'pattern',
      message: 'pattern'
    },
  ];
