import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PlannedDishesSimple, WeekDay } from '../../../../types/plan.type';
import { IngredientsCategory } from '../../../../types/ingredients.type';

@Component({
  selector: 'app-plan-item',
  templateUrl: './plan-item.component.html',
  styleUrls: ['./plan-item.component.scss']
})
export class PlanItemComponent {

  @Input()
  plan: PlannedDishesSimple;
  @Input()
  isExpanded: boolean = false;

  @Output()
  removePlan: EventEmitter<PlannedDishesSimple> = new EventEmitter<PlannedDishesSimple>();
  @Output()
  editPlan: EventEmitter<PlannedDishesSimple> = new EventEmitter<PlannedDishesSimple>();

  allDays: WeekDay[];
  allCategory: IngredientsCategory[];

  constructor() {
    this.allDays = Object.values(WeekDay);
    this.allCategory = Object.values(IngredientsCategory);
  }

  isCurrent(from: string | null, to: string | null): boolean {
    const now = new Date();
    const fromDate = new Date(`${from}T00:00:00` || '');
    const toDate = new Date(`${to}T23:59:59` || '');

    return (now <= toDate) && (now >= fromDate);
  }

  sumTime(): number {
    return this.plan?.dishes
      .map((planDish) => planDish?.dish?.timePreparing || 0)
      .reduce((prev, curr) => prev += curr, 0);
  }

  editDish() {
    this.editPlan.emit(this.plan);
  }

  removeDish() {
    this.removePlan.emit(this.plan);
  }
}
