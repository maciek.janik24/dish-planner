import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DishSimple, NewDishDialogData } from '../../../../types/dish.type';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IngredientSimple } from '../../../../types/ingredients.type';

@Component({
  selector: 'app-new-dish',
  templateUrl: './new-dish.component.html',
  styleUrls: ['./new-dish.component.scss']
})
export class NewDishComponent {
  isEdit: boolean;
  form: FormGroup;
  currentListIngredients: IngredientSimple[];
  filteredListIngredients: IngredientSimple[][] = [];

  constructor(public dialogRef: MatDialogRef<NewDishComponent>,
              @Inject(MAT_DIALOG_DATA) public data: NewDishDialogData,
              private formBuilder: FormBuilder) {
    this.isEdit = this.data.isEdit;
    this.currentListIngredients = this.data.ingredients;
    const currentDish = this.data.currentDish;

    this.form = this.formBuilder.group({
      name: [this.isEdit ? currentDish?.name : '', [Validators.required]],
      hours: [this.isEdit ? this.getHour(currentDish?.timePreparing) : '', [Validators.required, Validators.min(0)]],
      minutes: [this.isEdit ? this.getMinutes(currentDish?.timePreparing) : '', [Validators.required, Validators.min(0), Validators.max(59)]],
      ingredients: this.formBuilder.array([], Validators.required)
    });
    this.initIngredientFormArray(currentDish);
  }

  get ingredients(): FormArray {
    return <FormArray>this.form.get('ingredients');
  }

  getIngredientsFormGroup(index: number): FormGroup {
    return <FormGroup>this.ingredients.controls[index];
  }

  addNextIngredient(ingredient: IngredientSimple | undefined = undefined) {
    this.filteredListIngredients.push(this.currentListIngredients);
    this.ingredients.push(this.createIngredient(ingredient));
  }

  createIngredient(ingredient: IngredientSimple | undefined = undefined): FormGroup {
    return this.formBuilder.group({
      item: [ingredient ? ingredient : null, Validators.required]
    });
  }

  removeIngredientInput(i: number) {
    this.filteredListIngredients.splice(i, 1);
    this.ingredients.removeAt(i);
  }

  searchIngredients(event: any, index: number) {
    const searchValue = event.target.value;
    if (searchValue === '') {
      this.filteredListIngredients[index] = this.currentListIngredients;
    } else {
      this.filteredListIngredients[index] = this.currentListIngredients.filter((i) => i.name.includes(searchValue));
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  addNew() {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      this.dialogRef.close(this.getDishInput());
    }
  }

  private initIngredientFormArray(currentDish: DishSimple | undefined) {
    if (this.isEdit && currentDish != undefined) {
      currentDish.ingredients.forEach((dishIngred) =>
        this.addNextIngredient(
          this.currentListIngredients
            .find((curr) =>
              curr.name === dishIngred.name &&
              curr.category === dishIngred.category)
        ));
    } else {
      this.addNextIngredient();
    }
  }

  private getDishInput() {
    const hoursValue = this.form.controls['hours'].value;
    const minutesValue = this.form.controls['minutes'].value;
    const inputIngredients = this.ingredients.controls
      .map((i) => i.value.item as IngredientSimple);

    return {
      name: this.form.controls['name'].value,
      timePreparing: hoursValue * 3600 + minutesValue * 60,
      ingredients: inputIngredients
    };
  }

  private getHour(timePreparing: number | undefined) {
    if (timePreparing != undefined) {
      return Math.floor(timePreparing / 3600);
    }
    return 0;
  }

  private getMinutes(timePreparing: number | undefined) {
    if (timePreparing != undefined) {
      return Math.floor((timePreparing % 3600) / 60);
    }
    return 0;
  }
}
