import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PlannedDishesSimple } from '../../../types/plan.type';

@Component({
  selector: 'app-planned-list',
  templateUrl: './planned-list.component.html',
  styleUrls: ['./planned-list.component.scss']
})
export class PlannedListComponent {
  @Input()
  plans: PlannedDishesSimple[];
  @Input()
  onlyCurrent: boolean;

  @Output()
  removePlanEvent: EventEmitter<PlannedDishesSimple> = new EventEmitter<PlannedDishesSimple>();
  @Output()
  editPlanEvent: EventEmitter<PlannedDishesSimple> = new EventEmitter<PlannedDishesSimple>();


  getCurrentPlan(plannedList: PlannedDishesSimple[]): PlannedDishesSimple {
    const now = new Date();

    return plannedList.filter((plan) => {
      const fromDate = new Date(`${plan.from}T00:00:00` || '');
      const toDate = new Date(`${plan.to}T23:59:59` || '');

      return (now <= toDate) && (now >= fromDate);
    })[0];
  }

  sortPlan(plannedList: PlannedDishesSimple[]): PlannedDishesSimple[] {
    return plannedList.sort((a, b) => new Date(b.from).getTime() - new Date(a.from).getTime());
  }

  isPlansExits() {
    return this.plans != undefined && this.plans.length >= 1;
  }

  passEditEvent(plan: PlannedDishesSimple){
    this.editPlanEvent.emit(plan)
  }
  passRemoveEvent(plan: PlannedDishesSimple){
    this.removePlanEvent.emit(plan)
  }
}
