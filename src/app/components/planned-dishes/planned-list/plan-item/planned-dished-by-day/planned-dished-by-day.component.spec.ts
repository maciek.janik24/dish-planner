import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannedDishedByDayComponent } from './planned-dished-by-day.component';

describe('PlannedDishedByDayComponent', () => {
  let component: PlannedDishedByDayComponent;
  let fixture: ComponentFixture<PlannedDishedByDayComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlannedDishedByDayComponent]
    });
    fixture = TestBed.createComponent(PlannedDishedByDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
