import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NewIngredientDialogData } from '../../../../types/ingredients.type';

@Component({
  selector: 'app-new-ingredient',
  templateUrl: './new-ingredient.component.html',
  styleUrls: ['./new-ingredient.component.scss']
})
export class NewIngredientComponent {
  newItem: string;

  constructor(public dialogRef: MatDialogRef<NewIngredientComponent>,
              @Inject(MAT_DIALOG_DATA) public data: NewIngredientDialogData) {
  }

  cancel() {
    this.dialogRef.close();
  }

  addNew() {
    if (this.newItem != '' && this.newItem != undefined) {
      this.dialogRef.close(this.newItem);
    }
  }
}
