export type IngredientSimple = {
  name: string;
  category: IngredientsCategory;
}

export type IngredientCompressed = {
  id: string;
  ingredients: IngredientSimple[];
}

export interface NewIngredientDialogData {
  category: IngredientsCategory;
}

export enum IngredientsCategory {
  PIECZYWO = 'PIECZYWO',
  WARZYWA = 'WARZYWA',
  OWOCE = 'OWOCE',
  NABIAL = 'NABIAL',
  MIESO = 'MIESO',
  WEGLOWODANY = 'WEGLOWODANY',
  KONSERWY = 'KONSERWY',
  PRZYPRAWA_SUCHE = 'PRZYPRAWA_SUCHE',
  ZAMRAZALNIK = 'ZAMRAZALNIK',
  NAPOJE = 'NAPOJE',
  INNE = 'INNE',
}
