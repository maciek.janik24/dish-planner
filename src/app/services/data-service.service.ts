import { collection, collectionData, doc, Firestore, updateDoc } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DishesCompressed, DishSimple } from '../types/dish.type';
import { IngredientCompressed, IngredientSimple } from '../types/ingredients.type';
import { PlannedDishesSimple, PlansCompressed } from '../types/plan.type';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private db: Firestore) {
  }

  getAllDishes(): Observable<DishesCompressed[]> {
    let dishesCollection = collection(this.db, 'dishes-compressed');
    return collectionData(dishesCollection, { idField: 'id' }) as Observable<DishesCompressed[]>;
  }

  getAllPlan(): Observable<PlansCompressed[]> {
    let dishesCollection = collection(this.db, 'plans-compressed');
    return collectionData(dishesCollection, { idField: 'id' }) as Observable<PlansCompressed[]>;
  }

  getAllIngredientsCompressed(): Observable<IngredientCompressed[]> {
    let dishesCollection = collection(this.db, 'ingredients-compressed');
    return collectionData(dishesCollection, { idField: 'id' }) as Observable<IngredientCompressed[]>;
  }

  updateDishesCompressed(id: string, input: DishSimple[]): void {
    updateDoc(doc(this.db, 'dishes-compressed', id), { dishes: input });
  }

  updateIngredientsCompressed(id: string, input: IngredientSimple[]): void {
    updateDoc(doc(this.db, 'ingredients-compressed', id), { ingredients: input });
  }

  updatePlansCompressed(id: string, input: PlannedDishesSimple[]): void {
    updateDoc(doc(this.db, 'plans-compressed', id), { plans: input });
  }
}
