import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DishSimple } from '../../../../types/dish.type';
import { IngredientsCategory } from '../../../../types/ingredients.type';

@Component({
  selector: 'app-dishes-list-item',
  templateUrl: './dishes-list-item.component.html',
  styleUrls: ['./dishes-list-item.component.scss']
})
export class DishesListItemComponent {

  @Input()
  dish: DishSimple;
  @Input()
  isExpanded: boolean = false;

  @Output()
  remove: EventEmitter<DishSimple> = new EventEmitter<DishSimple>();
  @Output()
  edit: EventEmitter<DishSimple> = new EventEmitter<DishSimple>();

  allCategory: IngredientsCategory[];

  constructor() {
    this.allCategory = Object.values(IngredientsCategory);
  }

  removeDish() {
    this.remove.emit(this.dish);
  }

  editDish() {
    this.edit.emit(this.dish);
  }

}
