import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsByCategoryComponent } from './ingredients-by-category.component';

describe('IngredientsByCategoryComponent', () => {
  let component: IngredientsByCategoryComponent;
  let fixture: ComponentFixture<IngredientsByCategoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IngredientsByCategoryComponent]
    });
    fixture = TestBed.createComponent(IngredientsByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
