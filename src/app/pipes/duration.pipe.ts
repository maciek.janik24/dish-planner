import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration',
  standalone: true
})
export class DurationPipe implements PipeTransform {

  transform(value: number | undefined): string {
    if (value) {
      const hours = Math.floor(value / 3600);
      const minutes = Math.floor((value % 3600) / 60);
      const seconds = Math.floor(value % 60);

      return `${ this.getHour(hours) }${ this.getMinutes(minutes) }${ this.getSec(seconds) }`;
    } else {
      return '-';
    }

  }

  private getHour(hours: number): string {
    return hours !== 0 ? `${ hours }h ` : '';
  }

  private getMinutes(minutes: number): string {
    return minutes !== 0 ? `${ minutes }m ` : '';
  }

  private getSec(seconds: number): string {
    return seconds !== 0 ? `${ seconds }s` : '';
  }

}
