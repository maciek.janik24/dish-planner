import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { DataService } from './services/data-service.service';
import { HomeComponent } from './components/home/home.component';
import { PlannedPageComponent } from './components/planned-dishes/planned-page.component';
import { DishesListComponent } from './components/management/dishes-list/dishes-list.component';
import { MenuComponent } from './components/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManagementComponent } from './components/management/management.component';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { DurationPipe } from './pipes/duration.pipe';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { PlanItemComponent } from './components/planned-dishes/planned-list/plan-item/plan-item.component';
import { NewPlanComponent } from './components/planned-dishes/new-plan/new-plan.component';
import {
  PlannedDishedByDayComponent
} from './components/planned-dishes/planned-list/plan-item/planned-dished-by-day/planned-dished-by-day.component';
import {
  PlannedIngredientsComponent
} from './components/common/planned-ingredients/planned-ingredients.component';
import { PlannedListComponent } from './components/planned-dishes/planned-list/planned-list.component';
import { MatTabsModule } from '@angular/material/tabs';
import { IngredientsListComponent } from './components/management/ingredients-list/ingredients-list.component';
import {
  IngredientsByCategoryComponent
} from './components/management/ingredients-list/ingredients-by-category/ingredients-by-category.component';
import {
  NewIngredientComponent
} from './components/management/ingredients-list/new-ingredient/new-ingredient.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { getAnalytics, provideAnalytics } from '@angular/fire/analytics';
import { DishesListItemComponent } from './components/management/dishes-list/dishes-list-item/dishes-list-item.component';
import { NewDishComponent } from './components/management/dishes-list/new-dish/new-dish.component';
import { ErrorFormComponent } from './components/common/error-form/error-form.component';
import { MatSelectModule } from '@angular/material/select';
import { BoldDividerComponent } from './components/common/bold-divider/bold-divider.component';
import { ConfimationModalComponent } from './components/common/confimation-modal/confimation-modal.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

const firebaseConfig = {
  apiKey: 'AIzaSyDPgJqg6C56xZyd_4Ct6Q5bDNJw-SNZags',
  authDomain: 'dish-planner.firebaseapp.com',
  projectId: 'dish-planner',
  storageBucket: 'dish-planner.appspot.com',
  messagingSenderId: '249526855542',
  appId: '1:249526855542:web:62eb56b08efb41e4b1fc57',
  measurementId: 'G-KVHGT95XJM'
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlannedPageComponent,
    DishesListComponent,
    MenuComponent,
    ManagementComponent,
    PlanItemComponent,
    NewPlanComponent,
    PlannedDishedByDayComponent,
    PlannedIngredientsComponent,
    PlannedListComponent,
    IngredientsListComponent,
    IngredientsByCategoryComponent,
    NewIngredientComponent,
    DishesListItemComponent,
    NewDishComponent,
    ErrorFormComponent,
    BoldDividerComponent,
    ConfimationModalComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    provideFirebaseApp(() => initializeApp(firebaseConfig)),
    provideAnalytics(() => getAnalytics(initializeApp(firebaseConfig))),
    provideFirestore(() => getFirestore()),
    BrowserAnimationsModule,
    MatButtonModule,
    MatExpansionModule,
    DurationPipe,
    MatListModule,
    MatChipsModule,
    MatTabsModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
