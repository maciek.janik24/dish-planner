import { IngredientSimple } from './ingredients.type';

export type DishesCompressed = {
  id: string;
  dishes: DishSimple[];
}

export type DishSimple = {
  name: string;
  timePreparing: number;
  ingredients: IngredientSimple[];
}

export interface NewDishDialogData {
  isEdit: boolean;
  currentDish?:DishSimple;
  ingredients: IngredientSimple[];
}

