import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannedIngredientsComponent } from './planned-ingredients.component';

describe('PlannedIngredientsComponent', () => {
  let component: PlannedIngredientsComponent;
  let fixture: ComponentFixture<PlannedIngredientsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlannedIngredientsComponent]
    });
    fixture = TestBed.createComponent(PlannedIngredientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
