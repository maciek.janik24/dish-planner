import { Component, Input, OnInit } from '@angular/core';
import { IngredientsCategory, IngredientSimple } from '../../../types/ingredients.type';

@Component({
  selector: 'app-planned-ingredients',
  templateUrl: './planned-ingredients.component.html',
  styleUrls: ['./planned-ingredients.component.scss']
})
export class PlannedIngredientsComponent implements OnInit {

  @Input()
  category: IngredientsCategory;
  @Input()
  planIngredients: IngredientSimple[];

  currentIngredients: string[] = [];

  ngOnInit(): void {
    this.currentIngredients = this.getCurrentPlanIngredientsByCategory();
  }

  getCurrentPlanIngredientsByCategory(): string[] {
    return this.planIngredients
      .filter((ingredient) => ingredient.category === this.category)
      .map((ingredient) => ingredient.name)
      .filter((value, index, self) => self.indexOf(value) === index);
  }

}
