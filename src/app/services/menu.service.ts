import { BehaviorSubject, Observable } from 'rxjs';
import { MENU } from '../types/routing.type';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private activeRoute: BehaviorSubject<MENU> = new BehaviorSubject<MENU>(MENU.HOME);

  setNewRoute(route: MENU): void {
    this.activeRoute.next(route);
  }

  getActiveRoute(): Observable<MENU> {
    return this.activeRoute.asObservable();
  }
}
