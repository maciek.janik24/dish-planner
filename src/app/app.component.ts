import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PATH } from './types/routing.type';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Dish planner 🌭 🍕 🥗 🍔';

  constructor(private router: Router,
              private dateAdapter: DateAdapter<any>) {

    this.dateAdapter.setLocale('pl');
  }

  goToHome() {
    this.router.navigate([`/${ PATH.HOME }`]);
  }
}
