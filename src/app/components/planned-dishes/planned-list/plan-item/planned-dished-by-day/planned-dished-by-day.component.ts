import { Component, Input } from '@angular/core';
import { PlannedDish, WeekDay } from '../../../../../types/plan.type';

@Component({
  selector: 'app-planned-dished-by-day',
  templateUrl: './planned-dished-by-day.component.html',
  styleUrls: ['./planned-dished-by-day.component.scss']
})
export class PlannedDishedByDayComponent {

  @Input()
  day: WeekDay;
  @Input()
  dishes: PlannedDish[];
  otherDay: WeekDay = WeekDay.OTHER;

  getDishesByDay(): PlannedDish[] {
    return this.dishes.filter((dish) => dish.day === this.day);
  }

}
