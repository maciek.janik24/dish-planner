import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../../services/data-service.service';
import { DishSimple, DishesCompressed } from '../../../types/dish.type';
import { IngredientSimple } from '../../../types/ingredients.type';
import { MatDialog } from '@angular/material/dialog';
import { NewDishComponent } from './new-dish/new-dish.component';
import { ConfimationModalComponent } from '../../common/confimation-modal/confimation-modal.component';

@Component({
  selector: 'app-dishes-list',
  templateUrl: './dishes-list.component.html',
  styleUrls: ['./dishes-list.component.scss']
})
export class DishesListComponent implements OnInit {
  @Input()
  dishesCompressed: DishesCompressed;
  @Input()
  currentIngredients: IngredientSimple[];

  currentDishes: DishSimple[];
  currentId: string;

  constructor(public dialog: MatDialog, private data: DataService) {
  }

  ngOnInit(): void {
    this.currentDishes = this.sortDishes(this.dishesCompressed.dishes);
    this.currentId = this.dishesCompressed.id;
  }

  addNewDish(newItem: DishSimple) {
    this.currentDishes = this.sortDishes([...this.currentDishes, newItem]);
    this.data.updateDishesCompressed(this.currentId, this.currentDishes);
  }

  openCreationDishModal() {
    this.dialog.open(NewDishComponent, {
      height: '80%',
      width: '70%',
      data: { ingredients: this.currentIngredients, isEdit: false }
    }).afterClosed().subscribe((res) => {
      this.addNewDish(res);
    });
  }

  sortDishes(dishes: DishSimple[]) {
    return dishes.sort((a, b) =>
      (a.name.toLowerCase() < b.name.toLowerCase()) ?
        - 1 : (a.name.toLowerCase() > b.name.toLowerCase()) ?
          1 : 0);
  }

  removeDish(dishToRemove: DishSimple) {
    this.dialog.open(ConfimationModalComponent, {
      data: { actionName: 'removing DISH' },
    }).afterClosed().subscribe(res => {
        if (res) {
          this.currentDishes = this.currentDishes
            .filter((dish) => dish !== dishToRemove);

          this.data.updateDishesCompressed(this.currentId, this.currentDishes);
        }
      }
    );
  }

  editDishModal(dishToEdit: DishSimple) {
    this.dialog.open(NewDishComponent, {
      height: '80%',
      width: '70%',
      data: { ingredients: this.currentIngredients, isEdit: true, currentDish: dishToEdit }
    }).afterClosed().subscribe((res) => {
      if (res != undefined) {
        this.currentDishes = this.currentDishes
          .filter((dish) => dish !== dishToEdit);
        this.addNewDish(res);
      }
    });
  }
}
