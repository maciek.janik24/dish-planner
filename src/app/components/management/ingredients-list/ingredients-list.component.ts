import { Component, Input, OnInit } from '@angular/core';
import { IngredientCompressed, IngredientsCategory, IngredientSimple } from '../../../types/ingredients.type';
import { DataService } from '../../../services/data-service.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfimationModalComponent } from '../../common/confimation-modal/confimation-modal.component';

@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.scss']
})
export class IngredientsListComponent implements OnInit {

  @Input()
  ingredients: IngredientCompressed;

  allCategory: IngredientsCategory[];
  currentIngredients: IngredientSimple[];
  currentId: string;

  constructor(private data: DataService, public dialog: MatDialog) {
    this.allCategory = Object.values(IngredientsCategory);
  }

  ngOnInit(): void {
    this.currentIngredients = this.ingredients.ingredients;
    this.currentId = this.ingredients.id;
  }

  sortAndFilterItems(currentIngredients: IngredientSimple[], category: IngredientsCategory) {
    return currentIngredients
      .filter((item) => item.category === category)
      .sort((a, b) =>
        (a.name.toLowerCase() < b.name.toLowerCase()) ?
          - 1 : (a.name.toLowerCase() > b.name.toLowerCase()) ?
            1 : 0);
  }

  addNew(newItem: IngredientSimple) {
    if (newItem.name != '' && newItem.name != undefined) {
      this.currentIngredients.push(newItem);
      this.data.updateIngredientsCompressed(this.currentId, this.currentIngredients);
    }
  }

  removeIngredient(name: string) {
    this.dialog.open(ConfimationModalComponent, {
      data: { actionName: 'removing ITEM' },
    }).afterClosed().subscribe(res => {
        if (res) {
          this.currentIngredients = this.currentIngredients
            .filter((ingredient) => ingredient.name != name);

          this.data.updateIngredientsCompressed(this.currentId, this.currentIngredients);
        }
      }
    );
  }
}
