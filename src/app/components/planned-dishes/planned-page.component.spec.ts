import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannedPageComponent } from './planned-page.component';

describe('PlannedDishesComponent', () => {
  let component: PlannedPageComponent;
  let fixture: ComponentFixture<PlannedPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlannedPageComponent]
    });
    fixture = TestBed.createComponent(PlannedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
