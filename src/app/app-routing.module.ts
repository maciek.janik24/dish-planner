import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PlannedPageComponent } from './components/planned-dishes/planned-page.component';
import { ManagementComponent } from './components/management/management.component';
import { PATH } from './types/routing.type';

const routes: Routes = [
  { path: PATH.HOME, component: HomeComponent },
  { path: PATH.PLANNED_LIST, component: PlannedPageComponent },
  { path: PATH.MANAGE, component: ManagementComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
