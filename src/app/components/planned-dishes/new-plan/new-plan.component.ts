import { Component, Inject, OnInit } from '@angular/core';
import { MENU } from '../../../types/routing.type';
import { MenuService } from '../../../services/menu.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NewPlanDialogData, PlannedDish, PlannedDishesSimple, WeekDay } from '../../../types/plan.type';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DishSimple } from '../../../types/dish.type';
import { DatePipe } from '@angular/common';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-new-plan',
  templateUrl: './new-plan.component.html',
  styleUrls: ['./new-plan.component.scss'],
  providers: [DatePipe]
})
export class NewPlanComponent implements OnInit {
  allDays: WeekDay[];
  currentDishes: DishSimple[];
  filteredListDishes: DishSimple[][] = [];
  isEdit: boolean;
  form: FormGroup;
  otherDay: WeekDay = WeekDay.OTHER;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: NewPlanDialogData,
    public dialogRef: MatDialogRef<NewPlanComponent>,
    private formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<Date>,
    private menuService: MenuService,
    public datepipe: DatePipe
  ) {
    this.dateAdapter.getFirstDayOfWeek = () => 1;
    this.isEdit = this.data.isEdit;
    this.currentDishes = this.data.dishes;
    const currentPlan = this.data.currentPlan;
    this.allDays = Object.values(WeekDay);
    this.allDays.forEach((day, index) => {
      this.filteredListDishes[index] = this.currentDishes;
    });

    this.form = this.formBuilder.group({
      from: [this.isEdit ? currentPlan?.from : '', [Validators.required]],
      to: [this.isEdit ? currentPlan?.to : '', [Validators.required]],
      monday: [this.isEdit ? this.getDishForDay(WeekDay.MONDAY, currentPlan) : null],
      tuesday: [this.isEdit ? this.getDishForDay(WeekDay.TUESDAY, currentPlan) : null],
      wednesday: [this.isEdit ? this.getDishForDay(WeekDay.WEDNESDAY, currentPlan) : null],
      thursday: [this.isEdit ? this.getDishForDay(WeekDay.THURSDAY, currentPlan) : null],
      friday: [this.isEdit ? this.getDishForDay(WeekDay.FRIDAY, currentPlan) : null],
      saturday: [this.isEdit ? this.getDishForDay(WeekDay.SATURDAY, currentPlan) : null],
      sunday: [this.isEdit ? this.getDishForDay(WeekDay.SUNDAY, currentPlan) : null],
      other: [this.isEdit ? this.getDishForDay(WeekDay.OTHER, currentPlan) : null], //todo
    });
  }

  ngOnInit(): void {
    this.menuService.setNewRoute(MENU.PLAN);
  }

  cancel() {
    this.dialogRef.close();
  }

  clearSelection(day: WeekDay): void {
    this.form.controls[day.toLowerCase()].setValue(null);
  }

  isDaySelected(day: WeekDay): boolean {
    return this.form.controls[day.toLowerCase()].value;
  }

  addNew() {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      this.dialogRef.close(this.newPlanInput());
    }
  }

  searchDish(event: any, key: number) {
    const searchValue = event.target.value;
    if (searchValue === '') {
      this.filteredListDishes[key] = this.currentDishes;
    } else {
      this.filteredListDishes[key] = this.currentDishes.filter((dish) =>
        dish.name.toLowerCase().includes(searchValue.toLowerCase())
      );
    }
  }

  private getDishForDay(selectedDay: WeekDay, currentPlan: PlannedDishesSimple | undefined) {
    const dishFromPlanForSelectedDay = currentPlan?.dishes.find((dish) =>
      dish.day === selectedDay)?.dish || null;

    return this.currentDishes.find((currentDish) =>
      currentDish.name === dishFromPlanForSelectedDay?.name && currentDish.timePreparing === dishFromPlanForSelectedDay?.timePreparing) || null;
  }

  private newPlanInput(): PlannedDishesSimple {
    const dateFrom = this.form.controls['from'].value;
    const dateTo = this.form.controls['to'].value;

    return {
      from: this.datepipe.transform(dateFrom, 'YYYY-MM-dd') || '',
      to: this.datepipe.transform(dateTo, 'YYYY-MM-dd') || '',
      dishes: this.mapDishListFromForm()
    };
  }

  private mapDishListFromForm(): PlannedDish[] {
    const plannedDaysWithoutOthers = this.allDays
      .filter((day) => day !== WeekDay.OTHER)
      .map((day) =>
        ({
          day,
          dish: this.form.controls[day.toLowerCase()].value
        })
      ).filter((dishSimple) => dishSimple.dish);

    return [...plannedDaysWithoutOthers, ...this.getOthersSelectedDishes()];
  }

  private getOthersSelectedDishes() {
    return []; //todo lista obiektów kazdy z day OTHER
  }
}
