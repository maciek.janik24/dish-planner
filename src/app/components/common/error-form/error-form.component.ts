import { Component, Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { messageStore } from './messages';

@Component({
  selector: 'app-error-form',
  templateUrl: './error-form.component.html',
  styleUrls: ['./error-form.component.scss']
})
export class ErrorFormComponent {
  @Input() form: FormGroup;
  @Input() control: string;

  messages = messageStore;

  shouldShowErrors(control: AbstractControl | string, form: null | FormGroup = null) {
    let controlValue;
    if (!(control instanceof AbstractControl)) {
      controlValue = form?.controls[control];
    }
    if (controlValue != null && controlValue instanceof AbstractControl) {
      return controlValue.invalid && (controlValue.touched || controlValue.dirty);
    } else {
      console.warn('Control ' + control + ' not found when checking for errors');
      return false;
    }
  }

  getErrorMsgs(controlName: string, form: FormGroup): string[] {
    const control = form.get(controlName);
    const result: string[] = [];

    if (control != null && control.errors != null && Object.keys(control.errors).length > 0) {

      for (const errorName of Object.keys(control.errors)) {
        const foundMessage = this.messages.find((msg) => msg.key === errorName);

        if (foundMessage != undefined) {
          result.push(foundMessage.message);
        } else {
          result.push('Found unknown error');
        }
      }
    }
    return result;
  }

}
