import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { MENU } from '../../types/routing.type';
import { DataService } from '../../services/data-service.service';
import { Observable } from 'rxjs';
import { PlansCompressed } from '../../types/plan.type';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // @ts-ignore
  planned$: Observable<PlansCompressed[]>;

  constructor(private data: DataService, private menuService: MenuService) {
  }

  ngOnInit(): void {
    this.getData();
    this.menuService.setNewRoute(MENU.HOME);
  }

  getData(): void {
    this.planned$ = this.data.getAllPlan();
  }
}
