import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { MENU } from '../../types/routing.type';
import { Subscription } from 'rxjs';
import { IngredientCompressed, IngredientSimple } from '../../types/ingredients.type';
import { DataService } from '../../services/data-service.service';
import { DishesCompressed } from '../../types/dish.type';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit, OnDestroy {

  ingredients: IngredientCompressed;
  ingredientsListSorted: IngredientSimple[];
  dishes: DishesCompressed;

  private subs: Subscription = new Subscription();

  constructor(private menuService: MenuService, private data: DataService) {
  }

  ngOnInit(): void {
    this.menuService.setNewRoute(MENU.MANAGE);
    this.loadIngredients();
    this.loadDishes();
  }

  loadIngredients(): void {
    this.subs.add(
      this.data.getAllIngredientsCompressed()
        .subscribe((compressed) => {
          this.ingredients = compressed[0];
          this.ingredientsListSorted = compressed[0].ingredients.sort((a, b) =>
            (a.category.toLowerCase() < b.category.toLowerCase()) ?
              - 1 : (a.category.toLowerCase() > b.category.toLowerCase()) ?
                1 : 0);
        }));
  }

  loadDishes() {
    this.subs.add(
      this.data.getAllDishes().subscribe((res) => {
        this.dishes = res[0];
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
