import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { MENU } from '../../types/routing.type';
import { MatDialog } from '@angular/material/dialog';
import { NewPlanComponent } from './new-plan/new-plan.component';
import { Subscription } from 'rxjs';
import { NewPlanDialogData, PlannedDishesSimple } from '../../types/plan.type';
import { DataService } from '../../services/data-service.service';
import { DishSimple } from '../../types/dish.type';
import { ConfimationModalComponent } from '../common/confimation-modal/confimation-modal.component';

@Component({
  selector: 'app-planned-dishes',
  templateUrl: './planned-page.component.html',
  styleUrls: ['./planned-page.component.scss']
})
export class PlannedPageComponent implements OnInit, OnDestroy {
  currentId: string;
  dishesList: DishSimple[];
  plansList: PlannedDishesSimple[];

  private subs: Subscription = new Subscription();

  constructor(private data: DataService, public dialog: MatDialog, private menuService: MenuService) {
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  ngOnInit(): void {
    this.loadDishes();
    this.getData();
    this.menuService.setNewRoute(MENU.PLAN);
  }

  getData(): void {

    this.subs.add(
      this.data.getAllPlan()
        .subscribe((compressedPlans) => {
          this.currentId = compressedPlans[0].id;
          this.plansList = compressedPlans[0].plans;//todo add sort by dates
        })
    );
  }

  loadDishes(): void {
    this.subs.add(
      this.data.getAllDishes()
        .subscribe((compressed) => {
          this.dishesList = compressed[0].dishes.sort((a, b) =>
            (a.name.toLowerCase() < b.name.toLowerCase()) ?
              - 1 : (a.name.toLowerCase() > b.name.toLowerCase()) ?
                1 : 0);
        }));
  }

  goToNewPlan(): void {
    this.dialog.open(NewPlanComponent, {
      height: '80%',
      width: '70%',
      data: { isEdit: false, dishes: this.dishesList } as NewPlanDialogData
    }).afterClosed().subscribe((newPlan) => {
      if (newPlan != undefined) {
        this.data.updatePlansCompressed(this.currentId, [...this.plansList, newPlan]);
      }
    });
  }

  goToEditPlan(planToEdit: PlannedDishesSimple): void { //todo add output event
    this.dialog.open(NewPlanComponent, {
      height: '80%',
      width: '70%',
      data: { isEdit: true, dishes: this.dishesList, currentPlan: planToEdit } as NewPlanDialogData
    }).afterClosed().subscribe((newPlan) => {
      if (newPlan != undefined) {
        this.plansList = this.plansList
          .filter((dish) => dish !== planToEdit);
        this.data.updatePlansCompressed(this.currentId, [...this.plansList, newPlan]);
      }
    });
  }

  removePlan(planToRemove: PlannedDishesSimple) {
    this.dialog.open(ConfimationModalComponent, {
      data: { actionName: 'removing PLAN' },
    }).afterClosed().subscribe(res => {
        if (res) {
          this.plansList = this.plansList
            .filter((dish) => dish !== planToRemove);

          this.data.updatePlansCompressed(this.currentId, this.plansList);
        }
      }
    );
  }
}
