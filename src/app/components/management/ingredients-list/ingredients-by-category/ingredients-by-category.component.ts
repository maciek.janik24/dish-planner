import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IngredientsCategory, IngredientSimple } from '../../../../types/ingredients.type';
import { MatDialog } from '@angular/material/dialog';
import { NewIngredientComponent } from '../new-ingredient/new-ingredient.component';

@Component({
  selector: 'app-ingredients-by-category',
  templateUrl: './ingredients-by-category.component.html',
  styleUrls: ['./ingredients-by-category.component.scss']
})
export class IngredientsByCategoryComponent implements OnInit {

  @Input()
  category: IngredientsCategory;
  @Input()
  currentIngredients: IngredientSimple[];

  @Output()
  createNewItem = new EventEmitter<IngredientSimple>();
  @Output()
  removeItemEvent = new EventEmitter<string>();

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  openModal() {
    this.dialog.open(NewIngredientComponent, {
      data: { category: this.category },
    }).afterClosed().subscribe(res => {
        if (res != '' && res != undefined) {
          const payload: IngredientSimple = { name: res.toLowerCase(), category: this.category };
          this.createNewItem.emit(payload);
        }
      }
    );
  }

  removeItem(id: string) {
    this.removeItemEvent.emit(id);
  }
}
